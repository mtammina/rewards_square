//
//  cardListViewController.swift
//  bcentralNotableLite
//
//  Created by Tammina, Manoj on 9/8/18.
//  Copyright © 2018 Tammina. All rights reserved.
//

import UIKit

class cardListViewController: UIViewController , UITableViewDataSource, UITableViewDelegate{
    var creditCards = ["Amex","Chase ","Citi","Discover","Bofa"]
    var cardDetail = ["Bluecash","Saphirre","Costco rewards","Miles","Travel Credit"]
    var cardImages = ["amexlogo.png","chaselogo.jpg","citilogo.png","discoverlogo.jpg","bofalogo.jpeg"]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.creditCards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:myCustomCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! myCustomCell
        
            let cardsToShow = creditCards[indexPath.row]
            cell.cardTitle.text = cardsToShow
            cell.cardSubTitle.text = cardDetail[indexPath.row]
        cell.cardImage.image = UIImage(named:cardImages[indexPath.row])
        
//            cell.textLabel?.text = cardsToShow
//            cell.detailTextLabel?.text = cardDetail[indexPath.row]
        
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 75.0;//Choose your custom row height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0) // very light gray
        } else {
            cell.backgroundColor = UIColor.white
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
