//
//  locateMeViewController.swift
//  bcentralNotableLite
//
//  Created by Tammina, Manoj on 9/8/18.
//  Copyright © 2018 Tammina. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class locateMeViewController: UIViewController,CLLocationManagerDelegate {

    
    @IBOutlet weak var mapView: MKMapView!
    
    var locationManager : CLLocationManager!

    
    override func viewDidLoad() {
   
//        super.viewDidLoad()
        requestLocationAccess()

//        locationManager = CLLocationManager()
//        locationManager.delegate = self
//        locationManager.requestAlwaysAuthorization()
//
//        view.backgroundColor = UIColor.gray
        
//        if CLLocationManager.locationServicesEnabled() {
//            locationManager.delegate = self
//            locationManager.desiredAccuracy = kCLLocationAccuracyBest
//            locationManager.startUpdatingLocation()
//            locationManager.startMonitoringVisits()
//        }
//        else
//        {
//            locationManager.requestWhenInUseAuthorization()
//        }
        
//        mapView?.showsUserLocation = true
        //Required
//        mapView.delegate = self
//
//        let viewModel = YourCalloutViewModel(title: "Cafe", image: UIImage(named: "cafe.png")!)
//
//        let annotation = AnnotationPlus(viewModel: viewModel,
//                                        coordinate: CLLocationCoordinate2DMake(50.11, 8.68))
//
//        var annotations: [AnnotationPlus] = []
//        annotations.append(annotation)
//
//        mapView.setup(withAnnotations: annotations)
    }
    
    func requestLocationAccess() {
        let status = CLLocationManager.authorizationStatus()
        let authorizationStatus = CLLocationManager.authorizationStatus()
        
        if (authorizationStatus == CLAuthorizationStatus.notDetermined) || (authorizationStatus == nil) {
            locationManager.requestWhenInUseAuthorization()
        } else {
            locationManager.startUpdatingLocation()
        }
//        switch status {
//        case .authorizedAlways, .authorizedWhenInUse:
//            return
//
//        case .denied, .restricted:
//            print("location access denied")
//
//        default:
//            locationManager.requestWhenInUseAuthorization()
//        }
    }
    
    func locationManager(_ manager: CLLocationManager, didVisit visit: CLVisit) {
        print("Visit: \(visit)")
        // I find that sending this to a UILocalNotification is handy for debugging
    }
    
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    print(pm.country ?? "something country")
                    print(pm.locality ?? "something locality")
                    print(pm.subLocality ?? "Something subLocality")
                    print(pm.thoroughfare ?? "Something thouroughfare")
                    print(pm.postalCode ?? "Something postalcode")
                    print(pm.subThoroughfare ?? "Something subthoroughfare")
                    var addressString : String = ""
                    if pm.subThoroughfare != nil {
                        addressString = addressString + pm.subThoroughfare! + ", "
                    }
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    
                    print(addressString)
                }
        })
        
    }
    

}
