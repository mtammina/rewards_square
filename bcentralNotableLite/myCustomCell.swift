//
//  myCustomCell.swift
//  bcentralNotableLite
//
//  Created by Tammina, Manoj on 9/8/18.
//  Copyright © 2018 Tammina. All rights reserved.
//

import UIKit

class myCustomCell: UITableViewCell {

    @IBOutlet weak var cardTitle: UILabel!
    @IBOutlet weak var cardSubTitle: UILabel!
    @IBOutlet weak var cardImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
