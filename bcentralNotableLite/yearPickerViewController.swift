//
//  yearPickerViewController.swift
//  bcentralNotableLite
//
//  Created by Tammina, Manoj on 9/8/18.
//  Copyright © 2018 Tammina. All rights reserved.
//

import UIKit

class yearPickerViewController: UIViewController , UIPickerViewDelegate, UIPickerViewDataSource{
    @IBOutlet weak var picker: UIPickerView!
    let colors = ["Red","Yellow","Green","Blue"]
    var years = [String]()

//    var years:[Any] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        picker.dataSource = self
        for i in 1918...2000 {
//            print (i)
            var x = String(i)
            years.append(x)
//            years += String(i)
//            years[i] =String(i)
//            print(i)
        }
        
        picker.selectRow(78, inComponent: 0, animated: true)


        // Do any additional setup after loading the view.
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        let myYear = String(describing: years[row])
        return years[row]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return years.count
    }

    @IBAction func doneYear(_ sender: Any) {
        performSegue(withIdentifier: "sendYearToHome", sender: self)

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
